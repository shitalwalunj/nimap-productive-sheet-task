import { Directive ,ElementRef,HostListener,Input} from '@angular/core';

@Directive({
  selector: '[appChangeColor]'
})

export class ChangeColorDirective {

  constructor(private element:ElementRef) {  
    this.element.nativeElement.style.color='red';
    //this.element.nativeElement.style.color=color;
  }
  @Input() appChangeColor="";
  @HostListener('mouseenter')mouseEnter()
  {
    console.log("this.appChangeColor",this.appChangeColor);
    this.onclick(this.appChangeColor);
  }
  @HostListener('mouseleave')mouseLeave()
  {
    this.onclick("");
  }
  onclick(color)
  {
    this.element.nativeElement.style.color=color;
  }
}
