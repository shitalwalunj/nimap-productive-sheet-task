import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, NgForm, Validators,FormGroup,FormBuilder} from '@angular/forms';
import {ErrorStateMatcher} from '@angular/material/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../auth.service';
import {RegisterService} from '../sheard/register.service';
import { NumberDirective } from '../number.directive';
import { ToastrService } from 'ngx-toastr';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { map } from 'rxjs';
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public loginForm !:FormGroup;
  loading = false;
  submitted = false;
  ifLogin: string;

  matcher = new MyErrorStateMatcher();
  id: any;
  role: any;
  private returnUrl: any;
  constructor(private formBuilder:FormBuilder,
    private http:HttpClient,
    private router:Router,
    private register :RegisterService,
    private route: ActivatedRoute,
    private authService :AuthService,
    private toast :ToastrService) { }

  ngOnInit(): void {
    this.route.queryParams.subscribe(params => {
			this.returnUrl = params['returnUrl'] || '/';
		});

    this.loginForm=this.formBuilder.group({
      username:['',[Validators.required]],
      password:['',[Validators.required,]]
    });
    // if(localStorage.getItem('token')=='abcdefghijklmnopqrstuvwxyz')
    // {
    //   this.ifLogin='In';
    //   console.log('The User is Logged In');
    // }
    // else
    // {
    //   this.ifLogin='OUT';
    //   console.log('The User is Logged OUT');
    // }
  }
  get username ()
  {
    return this.loginForm.get('username');
  }
  get password ()
  {
    return this.loginForm.get('password');
  }

  
onSubmit(){
  const controls = this.loginForm.controls;
  if(this.loginForm.invalid){
    this.loginForm.markAllAsTouched();
    return
 }
 const authData = {
  username: controls['username'].value,
  password: controls['password'].value
};
 this.authService.getAuthentication(authData.username, authData.password)
 .subscribe(res =>{
  if(res.success){
    const currentUser = 'Basic ' + btoa(authData.username + ':' + authData.password);
    console.log(currentUser,'access token')
    localStorage.setItem('token',currentUser)
    this.router.navigate(['/home'])
    this.router.navigateByUrl(this.returnUrl);
		// location.reload();
  }
  else {
    // this.toast.error("Invalid Login Data")
    alert("Invalid Username or Password")
  }
  });
    this.loginForm.reset();
}

  
}
