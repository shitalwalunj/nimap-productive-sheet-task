import { Directive, ElementRef } from '@angular/core';

@Directive({
  selector: '[appNumber]',
  exportAs: 'appNumber'
})
export class NumberDirective {

  constructor(private _el :ElementRef) {
    this._el.nativeElement.value = this._el.nativeElement.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');
   }

}
