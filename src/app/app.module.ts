import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatInputModule} from '@angular/material/input';
import {MatFormFieldModule} from '@angular/material/form-field';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {MatButtonModule} from '@angular/material/button';
import {MatDividerModule} from '@angular/material/divider';
import {MatCardModule} from '@angular/material/card';
import { AuthGuard } from './auth.guard';
import { LocationStrategy } from '@angular/common';
import { CommonInterceptor } from './common.interceptor';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSelectModule } from '@angular/material/select';
import { MatNativeDateModule, MatOptionModule } from '@angular/material/core';
import {MatDatepickerModule} from'@angular/material/datepicker'
import { ToastrModule } from 'ngx-toastr';
import { NumberDirective } from './number.directive';
// import { CustomDatePipe } from './custom-pipe.pipe';
//import { ChangeColorDirective } from './home/change-color.directive';
//import { ForeachArrayComponent } from './foreach-array/foreach-array.component';
//import { SearchfilterPipe } from './filter/searchfilter.pipe';
//import { FilterComponent } from './filter/filter.component';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    NumberDirective,
    // CustomDatePipe,
    //ChangeColorDirective,
   // ForeachArrayComponent,
  //  FilterComponent,
  //  SearchfilterPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatFormFieldModule,
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatDividerModule,
    MatCardModule,
    HttpClientModule,
    MatDialogModule,
    MatSelectModule,
    MatOptionModule,
    MatDatepickerModule,
    MatNativeDateModule,
    ToastrModule.forRoot()
  ],
  providers: [{provide:HTTP_INTERCEPTORS,useClass:CommonInterceptor,multi:true}, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
