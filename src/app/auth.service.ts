import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient) { }

  getToken() {
    let token = JSON.parse(localStorage.getItem('token'));
    if (token) {
      return token;
    }
    console.log(token,'token')
  }
 
  isLoggedIn() {
    return localStorage.getItem('token');
  }

  logout(): Observable<any> {
    return this.http.post<any>(`/api/auth/logout`, {})
  }  

  // getAuthentication(data:any):Observable<any>{
  //   return this.http.post<any>(`${environment.apiUrl}/api/Account/authenticate`,data).pipe(
  //     map(res =>{
  //        console.log(environment.apiUrl,'api url')  
  //        console.log(res,'api res')
  //   }));}


  // addTask(taskDetails): Observable<any> {
  //   return this.http.post(`${environment.apiUrl}/api/Task/AssignTask`,taskDetails)
  //     .pipe(
  //       map(res => res)
  //     );
  // }

  // getMyTask(data){
  //   return this.http.post(`${environment.apiUrl}/api/Task/UserTasksAssignedToMe`,data)
  //   .pipe(map(res => res));
  // }

  getAuthentication(username: string, password: string):Observable<any>{
    var data = this.http.post<any>(`api/Account/authenticate?request=test`, 
    {
      "Username": username,
      "Password":password
    }, { headers: new HttpHeaders({"Content-Type": "application/json; charset=utf-8"}) });

    // var resul = data.pipe(map(res => {console.log(res);})).subscribe();

    return data;
  }

  addTask(taskDetails): Observable<any> {
    return this.http.post(`api/Task/AssignTask`,taskDetails)
      .pipe(
        map(res => res)
      );
  }

  getMyTask(details): Observable<any> {
    
    return this.http.post(`api/Task/UserTasksAssignedByMe`, details)
      .pipe(
        map(res => res)
      );
  }
  deleteTask(taskId: number): Observable<any> {
    return this.http.get('api/Task/DeleteTask?taskId=' + taskId)
      .pipe(
        map(res => res)
      );
  }
}
