import { ChangeDetectorRef, Component, ElementRef, Inject, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog} from '@angular/material/dialog';
import { MatDialogRef } from '@angular/material/dialog';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AuthService } from 'src/app/auth.service';
import { ToastrService } from 'ngx-toastr';
import { CustomDatePipe } from '../../../app/custom-pipe.pipe';
@Component({
  selector: 'app-add-task',
  templateUrl: './add-task.component.html',
  styleUrls: ['./add-task.component.css']
})
export class AddTaskComponent implements OnInit {
  @ViewChild('imageFileInput', { static: false }) imageFileInput: ElementRef;
  title ="Add Task"
  addTaskForm :FormGroup
  selectedIndex: number = 0;
  IntercomGroupIds: any =[];
  isActive: any;
  displayFileName: string = '';
  leadFilter: any;
  imageName: string = '';
  imageExt: string = '';
  current = new Date();
  viewLoading: boolean = false;
  constructor(
    public dialogRef: MatDialogRef<AddTaskComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private fb: FormBuilder,
    private dialog: MatDialog,
    private authService:AuthService,
    private chRef:ChangeDetectorRef,
    private toastr:ToastrService,
    private customDatePipe: CustomDatePipe,
  ) { }

  ngOnInit(): void {
    this.createForm();
  }



  createForm() {
    this.addTaskForm = this.fb.group({
      Id: [''],
      AssignedBy: 102,
      AssignedToUserId: [''],
      AssignedDate: [''],
      CompletedDate: [''],
      Description: ['', Validators.required],
      IntercomGroupIds: [this.IntercomGroupIds],
      IsActive: [this.isActive],
      Latitude: [''],
      Location: [''],
      Longitude: [''],
      Image: [''],
      MultimediaData: [''],
      MultimediaExtension: [''],
      MultimediaFileName: [''],
      MultimediaType: [''],
      Priority: [''],
      TaskEndDateDisplay: ['', Validators.required],
      TaskEndDate: [''],
      TaskDisplayOwners: [''],
      TaskOwners: [''],
      TaskStatus: [''],
      Title: ['', Validators.required],
      UserDisplayIds: ['', Validators.required],
      UserIds: [''],
      LeadId:['']
    })
  }

  openFile() {
    this.imageFileInput.nativeElement.click();
  }

  uploadFile(inputValue: any): void {
    if (inputValue.files[0] && inputValue.files[0].size < 2000000) {
      var file = inputValue.files[0];
      this.displayFileName = file.name;
      this.imageName = this.displayFileName.replace(/\\/g, "/");
      this.imageName = (/[^/]*$/.exec(this.imageName)[0]);
      this.imageExt = (/[^.]*$/.exec(this.imageName)[0]);
      this.imageName = this.imageName.substring(0, this.imageName.lastIndexOf('.'));
      var reader = new FileReader();
      reader.onload = (e: any) => {
        var binaryData = e.target.result;
        var base64String = btoa(binaryData);
        var imagePath = base64String;
        this.addTaskForm.patchValue({
          Image: imagePath
        });
      }
      reader.readAsBinaryString(file);
    } else if (inputValue.files[0] && inputValue.files[0].size > 2000000) {
       this.toastr.error('File size is greater than 2MB');
      this.chRef.detectChanges();
    }
  }
  
  removeFile() {
    this.imageFileInput.nativeElement.value = '';
    this.displayFileName = '';
    this.addTaskForm.patchValue({
      Image: '',
      MultimediaData: '',
      MultimediaExtension: '',
      MultimediaFileName: '',
      MultimediaType: ''
    });
  }
 

 
  tabChange(){

  }

  closeDialog(){
    this.dialogRef.close();
  }

  onSubmit(){
    const controls = this.addTaskForm.controls;
    if(this.addTaskForm.invalid){
      this.addTaskForm.markAllAsTouched()
      return
    }
    let customDate = this.customDatePipe.transform(controls['TaskEndDateDisplay'].value,0,'d MMM yyyy hh:mm a')
    controls['TaskEndDate'].setValue(customDate);
    this.viewLoading = true;
    this.addTaskForm.controls['IntercomGroupIds'].setValue([]);
    this.addTaskForm.controls['UserIds'].setValue([109]);
    // this.addTaskForm.controls['TaskEndDateDisplay'].setValue(customDate)
    this.authService.addTask(this.addTaskForm.value).subscribe(res=>{
      // this.toastr.success(res.success)
      this.toastr.success("Task Added")
      this.dialogRef.close(true);
    },err =>{
      this.toastr.error(err.error.message)
      this.dialogRef.close(false);
    }
    )
  }
}




