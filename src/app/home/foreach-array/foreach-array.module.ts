import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ForeachArrayRoutingModule } from './foreach-array-routing.module';
import { ForeachArrayComponent } from './foreach-array.component';


@NgModule({
  declarations: [
    ForeachArrayComponent
  ],
  imports: [
    CommonModule,
    ForeachArrayRoutingModule
  ]
})
export class ForeachArrayModule { }
