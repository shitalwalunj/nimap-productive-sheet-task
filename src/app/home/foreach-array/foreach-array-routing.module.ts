import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForeachArrayComponent } from './foreach-array.component';

const routes: Routes = [{ path: '', component: ForeachArrayComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ForeachArrayRoutingModule { }
