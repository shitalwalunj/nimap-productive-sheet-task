import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// import { ChangeColorDirective } from '../change-color.directive';
import { HomeRoutingModule } from './home-routing.module';
import { RouterModule } from '@angular/router';
import { ChildComponent } from './child/child.component';
import { ParentComponent } from './parent/parent.component';
import { MainComponent } from './main/main.component';
import { ChangeColorDirective } from '../change-color.directive';
import { LifeCycleComponent } from './life-cycle/life-cycle.component';
//import { SearchfilterPipe } from '../filter/searchfilter.pipe';
//import { FilterComponent } from '../filter/filter.component';
import {MatTableModule} from '@angular/material/table';
import {MatFormFieldModule} from '@angular/material/form-field';
//import { MatPaginatorModule } from '@angular/material';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MatInputModule } from '@angular/material/input';
import { DatatableComponent } from './datatable/datatable.component';
import { AddTaskComponent } from './add-task/add-task.component';
import { ReactiveFormsModule } from '@angular/forms';
import {MatTabsModule} from '@angular/material/tabs';
import { MatOptionModule } from '@angular/material/core';
import { MatSelectModule } from '@angular/material/select';
import {MatDatepickerModule} from'@angular/material/datepicker';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonInterceptor } from '../common.interceptor';

@NgModule({
  declarations: [
    ChildComponent,
    ParentComponent,
    ChangeColorDirective,
    MainComponent,
    LifeCycleComponent,
    DatatableComponent,
    AddTaskComponent,
    // FilterComponent,
     //SearchfilterPipe
  ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    MatTableModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatInputModule,
    ReactiveFormsModule,
    MatTabsModule,
    MatOptionModule,
    MatSelectModule,
    MatDatepickerModule,
    HttpClientModule,
    RouterModule.forChild([
     
      
    ])
  ],
  providers:[
    CommonInterceptor,
    {
      provide: HTTP_INTERCEPTORS,
			useClass: CommonInterceptor,
			multi: true
    }
  ]
})
export class HomeModule { }
