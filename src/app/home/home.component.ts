import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { RegisterModel } from './register.model';
import {RegisterService} from '../sheard/register.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  fname:any;
  lname:any;
  id:any;
  ifLogin: string;
  user:string;
  admin:string;
  supervisor:string;
  //color:string;
  adminRole:boolean=false;
  superviseRole:boolean=false;
  color = 'red';
  registerData:RegisterModel;
  max20="abcdefghijklmnopqrstuvwxyz";
  constructor(private http:HttpClient,private router:Router,private router1: ActivatedRoute,private registerService:RegisterService) { }

  ngOnInit(): void {
    console.log(this.id=localStorage.getItem('id'));
    this.getData();
    
    //localStorage.setItem('id',this.router1.snapshot.params['id']);
    //localStorage.setItem('role',res.role);
    // this.getRegister();

    // if(localStorage.getItem('token')=='abcdefghijklmnopqrstuvwxyz')
    // {
    //   this.ifLogin='In';
    //   console.log('The User is Logged In');
    // }
    // else
    // {
    //   this.ifLogin='OUT';
    //   console.log('The User is Logged OUT');
    // }
    // if(localStorage.getItem('role')=='admin')
    // {
    //   this.adminRole=true;
    //   console.log('this.adminRole=true;');
    // }
    // else if(localStorage.getItem('role')=='supervisor')
    // {
    //   this.superviseRole=true;
    //   console.log('this.superviseRole=true;');
    // }
    //this.color='yellow';
  }
  onSubmit()
  {
    localStorage.removeItem('isLogging');
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    localStorage.removeItem('id');
    this.router.navigate(['/login']);
  }
  getData()
  {
    this.registerService.getData(this.id)
    .subscribe(
      (res) => {
         this.fname=res.fname;
         this.lname=res.lname;
         this.registerData=res;
        console.log(this.fname);
      },
      (err) => {
        console.log(err);
     },);
  }
 
  buttonclick()
{
 this.router.navigate(['category'], {queryParams: {data : JSON.stringify(this.registerData)}});
}
}
