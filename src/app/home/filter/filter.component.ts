import { HttpClient } from '@angular/common/http';
import { Component, OnInit,ChangeDetectorRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { RegisterModel } from '../register.model';
import { AddTaskComponent } from '../add-task/add-task.component';
import {MatDialog} from '@angular/material/dialog'
import { AuthService } from 'src/app/auth.service';
import { map } from 'rxjs';
// import { TaskDataSource } from 'src/app/dataSource/task.datasource';
//import { RegisterModel } from '../home/register.model';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css']
})
export class FilterComponent implements OnInit {
  // dataSource: TaskDataSource;
  // displayedColumns = ['title', 'customerName', 'assignedBy', 'assignDate','dueDate'];
  RegisterModel:RegisterModel[];
  searchValue:string;
  id:string;
  adminRole:boolean=false;
  superviseRole:boolean=false;
  from: any;
  to: any;
  title: any;
  userId: any;
  isArchive: any;
  userIds: any;
  priority: any;
  StatusIds: any;
  FromDate: any;
  ToDate: any;
  SortColumn: any;
  SortOrder: any;
  TaskList =[];
  details:any;
  constructor(private http:HttpClient,private router:Router,
    private router1: ActivatedRoute ,
    private dialog :MatDialog,
    private chRef: ChangeDetectorRef,
    private authService :AuthService) { }

  // ngOnInit(): void {
  //   this.http.get('http://localhost:3000/posts/').subscribe((res:RegisterModel[])=>{
  //     this.RegisterModel=res;
  //     this.id=localStorage.getItem('id');
  //   });
  //   if(localStorage.getItem('role')=='admin')
  //   {
  //     this.adminRole=true;
  //     console.log('this.adminRole=true;');
  //   }
  //   else if(localStorage.getItem('role')=='supervisor')
  //   {
  //     this.superviseRole=true;
  //     console.log('this.superviseRole=true;');
  //   }
  // }
  // onSubmit()
  // {
  //   localStorage.removeItem('isLogging');
  //   this.router.navigate(['/login']);
  // }

  ngOnInit(): void {
    this.details ={
        "From": 1,
        "To": 10,
        "Title": "",
        "UserId": "",
        "IsArchive": false,
        "UserIds": [],
        "Priority": "",
        "TaskStatus": "",
        "FromDueDate": "",
        "ToDueDate": "",
        "SortByDueDate": "",
        "SortColumn": "",
        "SortOrder": ""
    }
   this.displayTaskList(this.details);
  }

 

  displayTaskList(details){
    this.authService.getMyTask(details).subscribe(res =>{
      this.TaskList = res.data.TaskList
      console.log(this.TaskList,'my task list')
    })
  }

  addTask(){
    const success = 'Task Addedd Successfully'
    const params = {
      Action:'Add',
      Button:'Add'
    }
    const dilogRef = this.dialog.open(AddTaskComponent, {data:params});
    dilogRef.afterClosed().subscribe(res => {
      if(!res){
        return
      }
      // console.log(success,'success')
      this.chRef.detectChanges();
    })
  }

  deleteTask(taskId: number) {
      const _successMessage = 'Task Deleted Successfully';
  
        this.authService.deleteTask(taskId)
          .pipe(map(res => {
            if (res.Status == 200) {
              alert(_successMessage)
            }
            // location.reload();
            this.displayTaskList(this.details);
          }))
          .subscribe();
     
    }
}
