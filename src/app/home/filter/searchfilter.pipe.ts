import { Pipe, PipeTransform } from '@angular/core';
import { RegisterModel } from '../register.model';
//import { RegisterModel } from '../home/register.model';

@Pipe({
  name: 'searchfilter',
})
export class SearchfilterPipe implements PipeTransform {

  transform(RegisterModels: RegisterModel[], searchValue: string): RegisterModel[] {
    if(!RegisterModels||!searchValue)
    {
    return RegisterModels;
  }
return RegisterModels.filter(registerModel=>
  registerModel.username.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()) ||
  registerModel.email.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase())
  || registerModel.password.toLocaleLowerCase().includes(searchValue.toLocaleLowerCase()));
}
}
