import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FilterRoutingModule } from './filter-routing.module';
import { FilterComponent } from './filter.component';
import { FormsModule } from '@angular/forms';
import { SearchfilterPipe } from './searchfilter.pipe';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CommonInterceptor } from 'src/app/common.interceptor';
import { CustomDatePipe } from '../../../app/custom-pipe.pipe';


@NgModule({
  declarations: [
    FilterComponent,
    SearchfilterPipe,CustomDatePipe
  ],
  imports: [
    CommonModule,
    FilterRoutingModule,
    FormsModule
  ],
  providers:[
    CommonInterceptor,
    {
      provide: HTTP_INTERCEPTORS,
			useClass: CommonInterceptor,
			multi: true
    },
  ]
})
export class FilterModule { }
