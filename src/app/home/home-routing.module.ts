import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
//import { CategoryComponent } from '../category/category.component';
//import { ProductComponent } from '../product/product.component';
import { ChildComponent } from './child/child.component';
//import { FilterComponent } from '../filter/filter.component';
import { HomeComponent } from './home.component';
import { MainComponent } from './main/main.component';
import { ParentComponent } from './parent/parent.component';
import { AuthGuard } from '../auth.guard';
import { HasRoleGuard } from '../has-role.guard';
import { LifeCycleComponent } from './life-cycle/life-cycle.component';
import { DatatableComponent } from './datatable/datatable.component';


const routes: Routes = [
  {
    path:'',
    component:HomeComponent,
    children:[
      {path:'', component:MainComponent},
      {path:'parent', component:ParentComponent},
      {path:'child', component:ChildComponent},
      {path:'life-cycle', component:LifeCycleComponent},
      {path:'datatable', component:DatatableComponent},
     //{path:'filter', component:FilterComponent},
     { path: 'foreach', loadChildren: () => import('./foreach-array/foreach-array.module').then(m => m.ForeachArrayModule) },
     { path: 'filter', loadChildren: () => import('./filter/filter.module').then(m => m.FilterModule) },
      //{path:'category', component:CategoryComponent},
      //{path:'product', component:ProductComponent},
      { path: 'category',canActivate: [AuthGuard,HasRoleGuard],data: { role: 'admin'}, loadChildren: () => import('./category/category.module').then(m => m.CategoryModule) },
      { path: 'product',canActivate: [AuthGuard,HasRoleGuard],data: { role: 'supervisor' }, loadChildren: () => import('./product/product.module').then(m => m.ProductModule) },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
