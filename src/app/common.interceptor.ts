import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpResponse
} from '@angular/common/http';
import { map, Observable } from 'rxjs';
import { AuthService } from './auth.service';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable()
export class CommonInterceptor implements HttpInterceptor {

  constructor(
    private authService: AuthService,
    private route: ActivatedRoute,
    private router: Router) { }
    
    
    intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
      const token = localStorage.getItem('token');
      if (token != null) {
        const tokenizedReq = req.clone({ headers: req.headers.set('Authorization', token) });
        return next.handle(tokenizedReq).pipe(map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            if (event.status === 401) {
              // this.facadeService.userLoggedOut();
              // this.router.navigateByUrl('core/login');
            }
          }
          return event;
        }));
      } else {
        // this.facadeService.userLoggedOut();
        // this.router.navigateByUrl('core/login');
      }
      return next.handle(req);
    }
}
